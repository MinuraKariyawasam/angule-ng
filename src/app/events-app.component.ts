import { Component } from '@angular/core';

@Component({
  selector: 'events-root',
  template: '<events-list></events-list>'
  
})
export class EventsAppComponent {
  title = 'ng-fundamentals';
}
