import { Component } from '@angular/core';

@Component({
    selector: 'events-list',
    templateUrl: './events.component.html'
})
export class EventsListComponent {
    event = {
        id: 1,
        name: 'Angular Connect',
        time: '10.00 am',
        location: {
            address: '1057 DT',
            city: 'London',
            country: 'England' 
        } 
    }
}